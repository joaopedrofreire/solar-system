#!/usr/bin/python
# -*- coding: utf-8 -*-
import rospy
import tf2_ros
import geometry_msgs.msg
import math

rospy.init_node("broadcast", anonymous=True)
rate = rospy.Rate(10)

broadcaster = tf2_ros.TransformBroadcaster()
planets = rospy.get_param('planets')

while not rospy.is_shutdown():
    for planet in planets:
        planet_name = planet['name']
        orbit_radius = planet['orbit_radius']

        t = geometry_msgs.msg.TransformStamped()
        t.header.frame_id = "Sun"
        t.child_frame_id = planet_name

        time = rospy.Time.now()
        rad = (time.to_sec() * math.pi) / (5*orbit_radius)
        x = orbit_radius * math.sin(rad)
        y = orbit_radius * math.cos(rad)

        t.transform.translation.x = x
        t.transform.translation.y = y
        t.transform.translation.z = 0
        t.transform.rotation.x = 0
        t.transform.rotation.y = 0
        t.transform.rotation.x = 0
        t.transform.rotation.y = 1

        broadcaster.sendTransform(t)

    rate.sleep()

rospy.spin()